namespace App_Data.DataAccess
{
	using DemoBook.Models.Domain;
	using System;
	using System.Data.Entity;
	using System.Linq;

	public class DataDb : DbContext
	{
		
		public DataDb()
			: base("name=DataDb")
		{
		}

		public virtual DbSet<User> Users { get; set; }

		public virtual DbSet<Book> Books { get; set; }

		public virtual DbSet<Category> Categorys { get; set; }


		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<User>()
				.HasMany(e => e.Books)
				.WithRequired(e => e.User)
				.WillCascadeOnDelete();

			

			modelBuilder.Entity<Book>()
				.HasRequired(e => e.Category);

			modelBuilder.Entity<User>()
			   .Property(e => e.Name)
			   .IsRequired()
			   .HasMaxLength(200);

			modelBuilder.Entity<User>()
			   .Property(e => e.PasswordHash)
			   .IsRequired()
			   .HasMaxLength(200);

			modelBuilder.Entity<Book>()
			   .Property(e => e.Name)
			   .IsRequired()
			   .HasMaxLength(1000);

            modelBuilder.Entity<Book>()
               .Property(e => e.Author)
               .IsRequired()
               .HasMaxLength(1000);

            modelBuilder.Entity<Category>()
			   .Property(e => e.Name)
			   .IsRequired()
			   .HasMaxLength(200);

			modelBuilder.Entity<User>()
				.HasKey(e => e.Id);

			modelBuilder.Entity<Book>()
				.HasKey(e => e.Id);

			modelBuilder.Entity<Category>()
				.HasKey(e => e.Id);
		}

	}

	
}