﻿using DemoBook.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoBook.BusinessLogic
{
	public interface IProjectService : IBookManager, ICategoryManager
    {
		bool Registration(UserRegistrationModel model);

        bool Login(UserRegistrationModel model);

        IEnumerable<CategoryModel> CategoryToList();
        
    }
}
