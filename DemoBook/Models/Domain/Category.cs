﻿using System.Collections.Generic;

namespace DemoBook.Models.Domain
{
	public class Category : BaseEntity
	{
		public string Name { get; set; }
		public virtual List<Book> Book { get; set; }
	}
}