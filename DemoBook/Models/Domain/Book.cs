﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoBook.Models.Domain
{
	public class Book : BaseEntity
	{
		public string Name { get; set; }

		public Guid UserId { get; set; }

		public Guid CategoryId { get; set; }

		public string Author { get; set; }

		public int Rating { get; set; }

		public virtual User User { get; set; }

		public virtual Category Category { get; set; }

		

	}
}