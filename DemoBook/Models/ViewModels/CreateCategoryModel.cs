﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoBook.Models.ViewModels
{
    public class CreateCategoryModel
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
}