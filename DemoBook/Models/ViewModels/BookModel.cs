﻿using DemoBook.Models.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoBook.Models.ViewModels
{
	public class BookModel
	{
		public Guid UserId { get; set; }


		[Display(Name = "Name")]
		public string Name { get; set; }

		[Display(Name = "Author")]
		public string Author { get; set; }


		public int Rating { get; set; }


		public Category Category { get; set; }
	}
}