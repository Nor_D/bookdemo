﻿using DemoBook.BusinessLogic;
using DemoBook.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoBook.Controllers
{
    public class CategoryController : Controller
    {

        private readonly IProjectService service;

        public CategoryController(IProjectService service)
        {
            this.service = service;
        }

        public ActionResult Index()
        {
            return View(service.CategoryToList());
        }

        [HttpGet, Route("Create")]
        public ActionResult Create()
        {
            return View();
        }

        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name")] CreateCategoryModel category)
        {
            if (ModelState.IsValid)
            {
                service.CreateCategory(category);
                return RedirectToAction("Index");
            }

            return View(category);
        }
    }
}