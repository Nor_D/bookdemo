﻿using DemoBook.Models.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoBook.Validators
{
    public class CreateBookValidator : AbstractValidator<CreateBookModel>
    {
        public CreateBookValidator()
        {
            this.RuleFor(e => e.Name)
                .NotEmpty().WithMessage("Name is required")
                .MaximumLength(1000).WithMessage("Maximum Length 1000");


            this.RuleFor(e => e.Author)
				.NotEmpty().WithMessage("Name is required")
				.MaximumLength(1000).WithMessage("Maximum Length 1000");



		}
    }
}