﻿using DemoBook.Validators;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoBook.Models.ViewModels
{
	[Validator(typeof(UserRegistrationValidator))]
	public class UserRegistrationModel
	{
		[Display(Name= "Name")]
		public string Name { get; set; }

		[Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
	}
}