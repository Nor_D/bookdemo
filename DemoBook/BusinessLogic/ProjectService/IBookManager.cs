﻿using DemoBook.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoBook.BusinessLogic
{
	public interface IBookManager
	{
		bool CreateBook(CreateBookModel model);

		void DeleteBook(Guid id);

        BookModel FindBook(Guid? id);

        bool UpdateBook(BookModel model);

		BookModel GetBookById(Guid id);

		List<BookModel> GetAllBookList();
	}
}
