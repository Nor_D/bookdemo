﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoBook.Models.Domain
{
	public class User : BaseEntity
	{

		public string Name { get; set; }
		public string PasswordHash { get; set; }

		public virtual List<Book> Books { get; set; }

	}
}