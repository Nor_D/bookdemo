﻿using DemoBook.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoBook.BusinessLogic
{
    public interface ICategoryManager
    {
        bool CreateCategory(CreateCategoryModel model);
    }
}
