﻿using App_Data.DataAccess;
using DemoBook.BusinessLogic;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DemoBook.DependencyRegistration
{
	public static class Dependencies
	{
		public static void Register(Container container)
		{
			container.Register<IProjectService, ProjectService>(Lifestyle.Scoped);

			container.Register<DbContext, DataDb>(Lifestyle.Scoped);
		}
	}
}