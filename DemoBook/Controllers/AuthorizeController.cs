﻿using DemoBook.BusinessLogic;
using DemoBook.Models.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace DemoBook.Controllers
{
	public class AuthorizeController : Controller
	{
		private readonly IProjectService service;

		public AuthorizeController(IProjectService service)
		{
			this.service = service;
		}

		[HttpGet, Route("Login")]
		public ActionResult Login()
		{
			return View();
		}

		[HttpPost, Route("Login")]
		public ActionResult Login(LoginModel model)
		{
			

			var ticket = this.CreateAuthenticationTicket(model);
			this.Authenticate(ticket);

			if (this.Request.UrlReferrer != null)
			{
				return this.Redirect(this.Request.UrlReferrer.AbsolutePath);
			}

			return this.RedirectToAction("Index", "Secret");
		}

		[HttpGet, Route("Registration")]
		public ActionResult Registration()
		{
			return View();
		}

		[HttpPost, Route("Registration")]
		public ActionResult Registration(UserRegistrationModel model)
		{
			if (!this.ModelState.IsValid)
			{
				return this.View(model);
			}
			if (service.Registration(model))
			{
				return this.Login();
			}
			
			ModelState.AddModelError("", "User whu have this name is exsist in Db" );
			return this.View(model);

		}


		private FormsAuthenticationTicket CreateAuthenticationTicket(LoginModel model)
		{
			int ticketVersion = 1;
			DateTime issueDate = DateTime.Now;
			DateTime validTo = DateTime.Now.AddHours(8);
			bool persistent = true;
			string userData = JsonConvert.SerializeObject(
				new CookieData
				{
					UserId = 1, // todo: assign real user ID
					Roles = new[] { "user", "admin", "superadmin" } // assign real roles
				});

			return new FormsAuthenticationTicket(
				ticketVersion,
				model.Name,
				issueDate,
				validTo,
				persistent,
				userData);
		}

		private void Authenticate(FormsAuthenticationTicket authTicket)
		{
			string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
			var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
			System.Web.HttpContext.Current.Response.Cookies.Add(authCookie);
		}

		
	}
	internal class CookieData
	{
		public long UserId { get; set; }
		public string[] Roles { get; set; }
	}
}