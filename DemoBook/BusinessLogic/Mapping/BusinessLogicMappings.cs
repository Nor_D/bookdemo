﻿using DemoBook.Models.Domain;
using DemoBook.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoBook.BusinessLogic.Mapping
{
	public static class BusinessLogicMappings
	{
		public static User ToUser(this UserRegistrationModel model)
		{
			return new User
			{
				Name = model.Name,
				PasswordHash = model.Password
			};
		}
        public static BookModel ToBookModel(this Book model)
        {
            return new BookModel
            {
                Name = model.Name,
                Author = model.Author,
                UserId = model.UserId,
                Rating = model.Rating
               
            };
        }
        public static Book ToBook(this CreateBookModel model)
        {
            return new Book
            {
                Name = model.Name,
                Author = model.Author,
                Rating = model.Rating,
                Category = model.Category
                
            };
        }

        public static Category ToCategory(this CreateCategoryModel model)
        {
            return new Category
            {
                Name = model.Name
            };
        }
        public static CategoryModel ToCategoryModel(this Category model)
        {
            return new CategoryModel
            {
                Name = model.Name,
                Id = model.Id
            };
        }
    }
}