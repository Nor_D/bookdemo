﻿using DemoBook.Models.Domain;
using DemoBook.Validators;
using FluentValidation.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace DemoBook.Models.ViewModels
{
	[Validator(typeof(CreateBookValidator))]
    public class CreateBookModel
    {

        public Guid IdUser { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Author")]
        public string Author { get; set; }

        [Display(Name = "Rating")]
        public int Rating { get; set; }

        [Display(Name = "Category")]
        [EnumDataType(typeof(String))]
        public Category Category { get; set; }
    }
}