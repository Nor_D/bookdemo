﻿using DemoBook.BusinessLogic;
using DemoBook.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DemoBook.Controllers
{
	public class BookManagerController : Controller
	{
		private readonly IProjectService service;

		public BookManagerController(IProjectService service)
		{
			this.service = service;
		}

		[HttpGet, Route("BookMenu")]
		public ActionResult BookMenu()
		{
			return View(service.GetAllBookList());
		}

		[HttpGet, Route("CreateBook")]
		public ActionResult CreateBook()
		{
			return View();
		}

		[HttpPost, Route("CreateBook")]
		public ActionResult CreateBook(CreateBookModel model)
		{
			if (!this.ModelState.IsValid)
			{
				return this.View(model);
			}
           // model.IdUser = HttpContext.User.Identity.Name

            if (service.CreateBook(model))
			{
				return this.BookMenu();
			}
			return this.View(model);
		}

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            service.FindBook(id);
            BookModel book = service.FindBook(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            service.DeleteBook(id);
            return RedirectToAction("Index");
        }

    }
}