﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using App_Data.DataAccess;
using DemoBook.BusinessLogic.Mapping;
using DemoBook.Models.Domain;
using DemoBook.Models.ViewModels;

namespace DemoBook.BusinessLogic
{
	public class ProjectService : IProjectService 
	{
		private readonly DbContext db;

		public ProjectService(DbContext dbContext)
		{
			db = dbContext;
		}
		public bool Registration(UserRegistrationModel model)
		{
			model.Password = Hashing(model.Password);
			var mabeUser = db.Set<User>().FirstOrDefault(q => q.Name == model.Name && q.PasswordHash == model.Password);
			if (mabeUser == null)
			{
				User user = model.ToUser();
				user.Id = Guid.NewGuid();
				db.Set<User>().Add(user);
				return true;
			}
			return false;
			
		}
        
		public static string Hashing(string password)
		{
			
			SHA256 sHA256 = SHA256Managed.Create();
			byte[] data = sHA256.ComputeHash(Encoding.UTF8.GetBytes(password));
			return ByteArrayToString(data);

		}
		public static string ByteArrayToString(byte[] ba)
		{
			StringBuilder hex = new StringBuilder(ba.Length * 2);
			foreach (byte b in ba)
				hex.AppendFormat("{0:x2}", b);
			return hex.ToString();
		}
        

        public bool Login(UserRegistrationModel model)
        {
            model.Password = Hashing(model.Password);
            var user = db.Set<User>().FirstOrDefault(q => q.Name == model.Name && q.PasswordHash == model.Password);
            return (user != null) ? true : false;
        }

		public bool CreateBook(CreateBookModel model)
		{
            try
            {
                Book obj = model.ToBook();
                obj.Id = Guid.NewGuid();
                db.Set<Book>().Add(obj);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

		public void DeleteBook(Guid id)
		{
            Book book = db.Set<Book>().Find(id);
            db.Set<Book>().Remove(book);
            db.SaveChanges();
        }

		public bool UpdateBook(BookModel model)
		{
			throw new NotImplementedException();
		}

		public BookModel GetBookById(Guid id)
		{
			throw new NotImplementedException();
		}

		public List<BookModel> GetAllBookList()
		{
            List<BookModel> list = new List<BookModel>();
            foreach (var element in db.Set<Book>())
            {
                BookModel book = element.ToBookModel();
                book.Category = db.Set<Category>().FirstOrDefault(e => e.Id == element.CategoryId);
                list.Add(book);
            }
            return list;
        }

        public bool CreateCategory(CreateCategoryModel model)
        {
            try
            {
                Category obj = model.ToCategory();
                obj.Id = Guid.NewGuid();
                db.Set<Category>().Add(obj);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
            
        }

        public IEnumerable<CategoryModel> CategoryToList()
        {
            List<CategoryModel> list = new List<CategoryModel>();
            foreach (var element in db.Set<Category>())
            {
                CategoryModel model = element.ToCategoryModel();              
                list.Add(model);
            }
            return list;
        }

        public void FindBook(Guid? id)
        {
            throw new NotImplementedException();
        }

        BookModel IBookManager.FindBook(Guid? id)
        {
            throw new NotImplementedException();
        }
    }
}