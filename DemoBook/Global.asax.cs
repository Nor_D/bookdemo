﻿using System;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using FluentValidation.Mvc;
using Newtonsoft.Json;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using DemoBook.DependencyRegistration;
using DemoBook.Controllers;

namespace DemoBook
{
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			Container container = new Container();

			RegisterWebDependencies(container);

			Dependencies.Register(container);

			container.Verify();

			AreaRegistration.RegisterAllAreas();
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);

			FluentValidationModelValidatorProvider.Configure();
		}

		protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
		{
			if (FormsAuthentication.CookiesSupported && this.Request.Cookies[FormsAuthentication.FormsCookieName] != null)
			{
				FormsAuthenticationTicket decryptedTicket = null;

				try
				{
					string encryptedCookie = this.Request.Cookies[FormsAuthentication.FormsCookieName].Value;
					decryptedTicket = FormsAuthentication.Decrypt(encryptedCookie);
				}
				catch
				{
					return;
				}

				if (decryptedTicket == null)
				{
					return;
				}

				CookieData data = JsonConvert.DeserializeObject<CookieData>(decryptedTicket.UserData);

				var identity = new GenericIdentity(decryptedTicket.Name, "Forms");
				HttpContext.Current.User = new GenericPrincipal(identity, data.Roles);
			}
		}

		private static void RegisterWebDependencies(Container container)
		{
			container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

			container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

			DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
		}
	}
}
