﻿using DemoBook.Models.Domain;
using DemoBook.Models.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoBook.Validators
{
    public class UserRegistrationValidator : AbstractValidator<UserRegistrationModel>
    {
        public UserRegistrationValidator()
        {

            this.RuleFor(x => x.Name)
                .NotEmpty().WithMessage("Name is required")
                .MaximumLength(200).WithMessage("Maximum Length 200");

            this.RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Password is required")
                .MaximumLength(200).WithMessage("Maximum Length 200");
        }
    }

    public class UserLoginValidator : AbstractValidator<LoginModel>
    {
        public UserLoginValidator()
        {
            this.RuleFor(x => x.Name)
                .NotEmpty().WithMessage("Name is required")
                .MaximumLength(200).WithMessage("Maximum Length 200");

            this.RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Password is required")
                .MaximumLength(200).WithMessage("Maximum Length 200");
        }
    }
}